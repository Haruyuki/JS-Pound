exports.run = (client, message, args) => {
  message.channel.send("A PM has been sent to you!");

  message.author.send("https://www.tailstar.us/")
    .catch(
      message.channel.send("A PM couldn't be sent to you, it may be that you have 'Allow direct messages from server members' disabled in your privacy settings.")
    );
};
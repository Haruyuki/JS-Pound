exports.run = (client, message, args) => {
  const request = require("superagent");
  const cheerio = require("cheerio");

  request.get("https://www.chickensmoothie.com/viewpet.php")
    .query({"id": 275516234})
    .end((err, res) => {
      if (err) return console.log(err);
      const $ = cheerio.load(res.text);
      console.log($("table[class=spine]").children().html());
    });
};